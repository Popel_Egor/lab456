#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>		/* time */
#include "VERYLONG.H"

using namespace std;

Verylong gcdex (Verylong a, Verylong b, Verylong & x, Verylong & y);
Verylong inverse(Verylong a, Verylong m);
Verylong binpow (Verylong a, Verylong n, Verylong m);

int main(int argc, char **argv)
{
	FILE *f;
	if (strcmp(argv[1], "help") == 0)
	{
		cout << "Files: p, q, g, y, w" << 
			"\nOptions:\n1) shnorr.exe peggi_1" << 
			"\n2) shnorr.exe victor_3" << 
			"\n3) shnorr.exe peggi_4" <<
			"\n4) shnorr.exe victor_5" << endl;
		return 0;
	}
	if (strcmp(argv[1], "peggi_1") == 0)
	{
		if (((f = fopen("p.txt", "r")) == NULL) || ((f = fopen("g.txt", "r")) == NULL) || ((f = fopen("q.txt", "r")) == NULL))
		{
			cout << "First create files: p, g, q" << endl;
			return 0;
		}
		char ps[10000], gs[10000], qs[10000];
		f = fopen("p.txt", "r");
		fscanf(f, "%s", &ps);
		fclose(f);
		f = fopen("g.txt", "r");
		fscanf(f, "%s", &gs);
		fclose(f);
		f = fopen("q.txt", "r");
		fscanf(f, "%s", &qs);
		fclose(f);
		Verylong p(ps), g(gs), q(qs);

		srand (time(NULL));
		int k = rand();
		Verylong r(k);
		r = r % (q - (Verylong)2) + (Verylong)1;

		Verylong x = binpow(g, r, p);

		cout << "result in file \'x.txt\', \'r.txt\'" << endl;
		freopen("x.txt", "w", stdout);
		cout << x;
		fclose(stdout);

		freopen("r.txt", "w", stdout);
		cout << r;
		fclose(stdout);
		
		return 0;
	}
	if (strcmp(argv[1], "victor_3") == 0)
	{
		if ((f = fopen("p.txt", "r")) == NULL)
		{
			cout << "First create files." << endl;
			return 0;
		}
		char ps[10000];
		f = fopen("p.txt", "r");
		fscanf(f, "%s", &ps);
		fclose(f);
		Verylong p(ps);

		srand (time(NULL));
		int r = rand();
		Verylong e(r);
		e = e % (p - (Verylong)2) + (Verylong)1;

		cout << "result in files \'e.txt\'" << endl;
		freopen("e.txt", "w", stdout);
		cout << e;
		fclose(stdout);
		
		return 0;
	}
	if (strcmp(argv[1], "peggi_4") == 0)
	{
		if (((f = fopen("r.txt", "r")) == NULL) || ((f = fopen("w.txt", "r")) == NULL) || ((f = fopen("e.txt", "r")) == NULL) || ((f = fopen("q.txt", "r")) == NULL))
		{
			cout << "First create files: r, w, e, q" << endl;
			return 0;
		}
		char rs[10000], ws[10000], es[10000], qs[10000];
		f = fopen("r.txt", "r");
		fscanf(f, "%s", &rs);
		fclose(f);
		f = fopen("w.txt", "r");
		fscanf(f, "%s", &ws);
		fclose(f);
		f = fopen("e.txt", "r");
		fscanf(f, "%s", &es);
		fclose(f);
		f = fopen("q.txt", "r");
		fscanf(f, "%s", &qs);
		fclose(f);
		Verylong r(rs), w(ws), e(es), q(qs);

		Verylong s = (w * e + r) % q;

		cout << "result in file \'s.txt\'" << endl;
		freopen("s.txt", "w", stdout);
		cout << s;
		fclose(stdout);
		
		return 0;
	}
	if (strcmp(argv[1], "victor_5") == 0)
	{
		if (((f = fopen("x.txt", "r")) == NULL) || ((f = fopen("g.txt", "r")) == NULL) || ((f = fopen("s.txt", "r")) == NULL) || ((f = fopen("y.txt", "r")) == NULL) || ((f = fopen("e.txt", "r")) == NULL) || ((f = fopen("p.txt", "r")) == NULL))
		{
			cout << "First create files: x, g, s, y, e, p" << endl;
			return 0;
		}
		char xs[10000], gs[10000], ss[10000], ys[10000], es[10000], ps[10000];
		f = fopen("x.txt", "r");
		fscanf(f, "%s", &xs);
		fclose(f);
		f = fopen("g.txt", "r");
		fscanf(f, "%s", &gs);
		fclose(f);
		f = fopen("s.txt", "r");
		fscanf(f, "%s", &ss);
		fclose(f);
		f = fopen("y.txt", "r");
		fscanf(f, "%s", &ys);
		fclose(f);
		f = fopen("e.txt", "r");
		fscanf(f, "%s", &es);
		fclose(f);
		f = fopen("p.txt", "r");
		fscanf(f, "%s", &ps);
		fclose(f);
		Verylong x(xs), g(gs), s(ss), y(ys), e(es), p(ps);

		Verylong x1 = binpow(g, s, p);
		Verylong x2 = binpow(y, e, p);
		Verylong x3 = (x1 * x2) % p;

		if (x == x3)
			cout << "\nSUCCESS!" << endl;
		else
			cout << "\nFAIL" << endl;
		
		return 0;
	}
}

Verylong gcdex (Verylong a, Verylong b, Verylong & x, Verylong & y)
{
	if (a == (Verylong)0)
	{
		x = 0;
		y = 1;
		return b;
	}
	Verylong x1, y1;
	Verylong d = gcdex (b%a, a, x1, y1);
	Verylong x2;
	if (b / a == (Verylong)0)
		x2 = (Verylong)10;
	else
		x2 = (b / a) * x1;
	Verylong ten = (Verylong)10;
	for (int i = 0; i < x2.vlen; i++)
		ten = ten * (Verylong)10;
	x = y1 + ten;
	x2 = x2 + ten;
	x = x - x2;
	y = x1;
	return d;
}

Verylong inverse(Verylong a, Verylong m)
{
	Verylong x, y;
	Verylong g = gcdex (a, m, x, y);
	if (g != (Verylong)1)
		return 0;
	else
	{
		x = (x % m + m) % m;
		return x;
	}
}

Verylong binpow (Verylong a, Verylong n, Verylong m)
{
	if (n == (Verylong)0)
		return (Verylong)1;
	if (n % (Verylong)2 == (Verylong)1)
		return (binpow (a, n-(Verylong)1, m) * a) % m;
	else {
		Verylong b = binpow (a, n/(Verylong)2, m);
		return (b * b) % m;
	}
}