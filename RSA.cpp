#include <iostream>
#include "VERYLONG.H"

using namespace std;

Verylong gcdex (Verylong a, Verylong b, Verylong & x, Verylong & y);
Verylong inverse(Verylong a, Verylong m);
Verylong phi (Verylong n);
Verylong binpow (Verylong a, Verylong n, Verylong m);

int main(int argc, char **argv)
{
	FILE *f;
	if (strcmp(argv[1], "help") == 0)
	{
		cout << "1) RSA.exe keys p.txt q.txt" <<
			"\n2) RSA.exe enc m.txt" << 
			"\n3) RSA.exe dec c.txt" << endl;
		return 0;
	}
	if (strcmp(argv[1], "keys") == 0)
	{
		if ((f = fopen("primes.txt", "r")) == NULL)
		{
			f = fopen("primes.txt", "w");
			bool primes[3500];
			for (int i = 2; i < 3400; i++)
				primes[i] = true;
			for (int i = 2; i < 3400; i++)
				if (primes[i])
					for (int j = i + i; j < 3400; j = j + i)
						primes[j] = false;
			for (int i = 2; i < 3400; i++)
				if (primes[i])
					fprintf(f, "%d ", i);
			fclose(f);
		}
		char ps[10000], qs[10000];
		f = fopen(argv[2], "r");
	//	f = fopen("p.txt", "r");
		fscanf(f, "%s", &ps);
		fclose(f);
		f = fopen(argv[3], "r");
	//	f = fopen("q.txt", "r");
		fscanf(f, "%s", &qs);
		fclose(f);
		Verylong p(ps), q(qs);
		Verylong n = p * q;
		Verylong phin = (p - (Verylong)1) * (q - (Verylong)1);
		Verylong e = 0;
		f = fopen("primes.txt", "r");
		while (!feof(f))
		{
			char temps[10];
			fscanf(f, "%s", &temps);
			Verylong temp(temps);
			if (phin % temp != (Verylong)0)
			{
				e = temp;
				break;
			}
		}
		fclose(f);

		/*Verylong pp = phi(phin);
		pp = pp - (Verylong)1;
		Verylong d = binpow(e, pp, phin);*/

		Verylong d = inverse(e, phin);

		freopen("e.txt", "w", stdout);
		cout << e;
		fclose(stdout);

		freopen("d.txt", "w", stdout);
		cout << d;
		fclose(stdout);

		freopen("n.txt", "w", stdout);
		cout << n;
		fclose(stdout);

		return 0;
	}
	if (strcmp(argv[1], "enc") == 0)
	{
		if (((f = fopen("e.txt", "r")) == NULL) || ((f = fopen(argv[2], "r")) == NULL) || ((f = fopen("n.txt", "r")) == NULL))
		{
			cout << "First create keys." << endl;
			return 0;
		}
		char es[10000], ns[10000], ms[10000];
		f = fopen("e.txt", "r");
		fscanf(f, "%s", &es);
		fclose(f);
		f = fopen(argv[2], "r");
		fscanf(f, "%s", &ms);
		fclose(f);
		f = fopen("n.txt", "r");
		fscanf(f, "%s", &ns);
		fclose(f);
		Verylong e(es), n(ns), m(ms);

		Verylong c = binpow(m, e, n);

		cout << "result in file \'c.txt\'" << endl;
		freopen("c.txt", "w", stdout);
		cout << c;
		fclose(stdout);
		
		return 0;
	}
	if (strcmp(argv[1], "dec") == 0)
	{
		if (((f = fopen(argv[2], "r")) == NULL) || ((f = fopen("d.txt", "r")) == NULL) || ((f = fopen("n.txt", "r")) == NULL))
		{
			cout << "First create keys." << endl;
			return 0;
		}
		char ds[10000], ns[10000], cs[10000];
		f = fopen("d.txt", "r");
		fscanf(f, "%s", &ds);
		fclose(f);
		f = fopen(argv[2], "r");
		fscanf(f, "%s", &cs);
		fclose(f);
		f = fopen("n.txt", "r");
		fscanf(f, "%s", &ns);
		fclose(f);
		Verylong d(ds), n(ns), c(cs);

		Verylong m = binpow(c, d, n);

		cout << "result in file \'m_.txt\'" << endl;
		freopen("m_.txt", "w", stdout);
		cout << m;
		fclose(stdout);
		
		return 0;
	}
}

Verylong gcdex (Verylong a, Verylong b, Verylong & x, Verylong & y)
{
	if (a == (Verylong)0)
	{
		x = 0;
		y = 1;
		return b;
	}
	Verylong x1, y1;
	Verylong d = gcdex (b%a, a, x1, y1);
	Verylong x2;
	if (b / a == (Verylong)0)
		x2 = (Verylong)10;
	else
		x2 = (b / a) * x1;
	Verylong ten = (Verylong)10;
	for (int i = 0; i < x2.vlen; i++)
		ten = ten * (Verylong)10;
	x = y1 + ten;
	x2 = x2 + ten;
	x = x - x2;
	y = x1;
	return d;
}

Verylong inverse(Verylong a, Verylong m)
{
	Verylong x, y;
	Verylong g = gcdex (a, m, x, y);
	if (g != (Verylong)1)
		return 0;
	else
	{
		x = (x % m + m) % m;
		return x;
	}
}


Verylong phi (Verylong n)
{
	Verylong result = n;
	for (Verylong i = (Verylong)2; i * i <= n; i++)
		if (n % i == (Verylong)0)
		{
			while (n % i == (Verylong)0)
				n /= i;
			result -= result / i;
		}
	if (n > (Verylong)1)
		result -= result / n;
	return result;
}

Verylong binpow (Verylong a, Verylong n, Verylong m)
{
	if (n == (Verylong)0)
		return (Verylong)1;
	if (n % (Verylong)2 == (Verylong)1)
		return (binpow (a, n-(Verylong)1, m) * a) % m;
	else {
		Verylong b = binpow (a, n/(Verylong)2, m);
		return (b * b) % m;
	}
}