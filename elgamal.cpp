#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>		/* time */
#include "VERYLONG.H"

using namespace std;

Verylong gcdex (Verylong a, Verylong b, Verylong & x, Verylong & y);
Verylong inverse(Verylong a, Verylong m);
Verylong phi (Verylong n);
Verylong binpow (Verylong a, Verylong n, Verylong m);

int main(int argc, char **argv)
{
	FILE *f;
	if (strcmp(argv[1], "help") == 0)
	{
		cout << "To encrypt you need files:\np.txt, g.txt, y.txt, m.txt" << 
			"\nTo decrypt you need files:\np.txt, g.txt, x.txt, m.txt" << 
			"\nOptions:\n1)elgamal.exe key" << 
			"\n2) elgamal.exe enc" << 
			"\n3) elgamal.exe dec" << endl;
		return 0;
	}
	if (strcmp(argv[1], "key") == 0)
	{
		if (((f = fopen("p.txt", "r")) == NULL) || ((f = fopen("g.txt", "r")) == NULL) || ((f = fopen("x.txt", "r")) == NULL))
		{
			cout << "First create files." << endl;
			return 0;
		}
		char ps[10000], gs[10000], xs[10000];
		f = fopen("p.txt", "r");
		fscanf(f, "%s", &ps);
		fclose(f);
		f = fopen("g.txt", "r");
		fscanf(f, "%s", &gs);
		fclose(f);
		f = fopen("x.txt", "r");
		fscanf(f, "%s", &xs);
		fclose(f);
		Verylong p(ps), g(gs), x(xs);

		Verylong y = binpow(g, x, p);

		cout << "result in file \'y.txt\'" << endl;
		freopen("y.txt", "w", stdout);
		cout << y;
		fclose(stdout);
		
		return 0;
	}
	if (strcmp(argv[1], "enc") == 0)
	{
		if (((f = fopen("p.txt", "r")) == NULL) || ((f = fopen("g.txt", "r")) == NULL) || ((f = fopen("y.txt", "r")) == NULL) || ((f = fopen("m.txt", "r")) == NULL))
		{
			cout << "First create files." << endl;
			return 0;
		}
		char ps[10000], gs[10000], ys[10000], ms[10000];
		f = fopen("p.txt", "r");
		fscanf(f, "%s", &ps);
		fclose(f);
		f = fopen("g.txt", "r");
		fscanf(f, "%s", &gs);
		fclose(f);
		f = fopen("y.txt", "r");
		fscanf(f, "%s", &ys);
		fclose(f);
		f = fopen("m.txt", "r");
		fscanf(f, "%s", &ms);
		fclose(f);
		Verylong p(ps), g(gs), m(ms), y(ys);

	//	Verylong y = binpow(g, x, p);

		srand (time(NULL));
		int r = rand();
		Verylong k(r);
		k = k % (p - (Verylong)2) + (Verylong)1;

		Verylong a = binpow(g, k, p);
		Verylong b = binpow(y, k, p);
		b = (b * m) % p;

		cout << "result in files \'a.txt\' \'b.txt\'" << endl;
		freopen("a.txt", "w", stdout);
		cout << a;
		fclose(stdout);

		freopen("b.txt", "w", stdout);
		cout << b;
		fclose(stdout);
		
		return 0;
	}
	if (strcmp(argv[1], "dec") == 0)
	{
		if (((f = fopen("a.txt", "r")) == NULL) || ((f = fopen("b.txt", "r")) == NULL) || ((f = fopen("x.txt", "r")) == NULL) || ((f = fopen("p.txt", "r")) == NULL))
		{
			cout << "First encrypt smth" << endl;
			return 0;
		}
		char as[10000], bs[10000], xs[10000], ps[10000];
		f = fopen("a.txt", "r");
		fscanf(f, "%s", &as);
		fclose(f);
		f = fopen("b.txt", "r");
		fscanf(f, "%s", &bs);
		fclose(f);
		f = fopen("x.txt", "r");
		fscanf(f, "%s", &xs);
		fclose(f);
		f = fopen("p.txt", "r");
		fscanf(f, "%s", &ps);
		fclose(f);
		Verylong a(as), b(bs), x(xs), p(ps);

		Verylong axmodp = binpow(a, x, p);
		/*Verylong pp = phi(p);
		pp = pp - (Verylong)1;
		Verylong t = binpow(axmodp, pp, p);*/
		Verylong t = inverse(axmodp, p);
		t = (t * b) % p;

		cout << "result in file \'m_.txt\'" << endl;
		freopen("m_.txt", "w", stdout);
		cout << t;
		fclose(stdout);
		
		return 0;
	}
}

Verylong gcdex (Verylong a, Verylong b, Verylong & x, Verylong & y)
{
	if (a == (Verylong)0)
	{
		x = 0;
		y = 1;
		return b;
	}
	Verylong x1, y1;
	Verylong d = gcdex (b%a, a, x1, y1);
	Verylong x2;
	if (b / a == (Verylong)0)
		x2 = (Verylong)10;
	else
		x2 = (b / a) * x1;
	Verylong ten = (Verylong)10;
	for (int i = 0; i < x2.vlen; i++)
		ten = ten * (Verylong)10;
	x = y1 + ten;
	x2 = x2 + ten;
	x = x - x2;
	y = x1;
	return d;
}

Verylong inverse(Verylong a, Verylong m)
{
	Verylong x, y;
	Verylong g = gcdex (a, m, x, y);
	if (g != (Verylong)1)
		return 0;
	else
	{
		x = (x % m + m) % m;
		return x;
	}
}

Verylong phi (Verylong n)
{
	Verylong result = n;
	for (Verylong i = (Verylong)2; i * i <= n; i++)
		if (n % i == (Verylong)0)
		{
			while (n % i == (Verylong)0)
				n /= i;
			result -= result / i;
		}
	if (n > (Verylong)1)
		result -= result / n;
	return result;
}

Verylong binpow (Verylong a, Verylong n, Verylong m)
{
	if (n == (Verylong)0)
		return (Verylong)1;
	if (n % (Verylong)2 == (Verylong)1)
		return (binpow (a, n-(Verylong)1, m) * a) % m;
	else {
		Verylong b = binpow (a, n/(Verylong)2, m);
		return (b * b) % m;
	}
}